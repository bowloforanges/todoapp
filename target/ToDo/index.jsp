    <%
    	String contextPath = request.getContextPath();
    	String message = "Choose what to do:";
    	String title = "To Do List!";
    %>
    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title><%= title %></title>
</head>
<body>
	<%= message %>
	<ul>
    	<hr><br>
    	<li><a href="<%= contextPath %>/views/insertForm.jsp">Insert a new Task</a></li>
    	<li><form method="POST" action="./TDC"><input type="submit" value="To Do List"></form></li>
    	<li><form method="POST" action="./DC"><input type="submit" value="Done List"></form></li>
    </ul>
    

</body>
</html>