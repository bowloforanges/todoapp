package com.softtek.course.javaweb.service;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import com.softtek.course.javaweb.beans.TaskBean;

public class ToDoService {

	private static Connection conn = null;

	private ToDoService() {

		String url = "jdbc:mysql://localhost:3306/examapp" + 
		"?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
		String driver = "com.mysql.jdbc.Driver";
		String user = "root";
		String password = "1234";

		try {
			Class.forName(driver);
			conn = DriverManager.getConnection(url, user, password);
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public static Connection getConnection() {

		if (conn == null) {
			new ToDoService();
		}
		return conn;
	}

	public static List<TaskBean> getAllToDo() {
		// returns undone tasks query
		TaskBean task = null;
		List<TaskBean> list = new ArrayList<TaskBean>();

		try {
			Connection con = getConnection();
			PreparedStatement ps = con.prepareStatement("SELECT * FROM TO_DO_LIST;");
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				task = new TaskBean();
				task.setId(rs.getInt("ID"));
				task.setList(rs.getString("LIST"));
				task.setIs_done(rs.getBoolean("IS_DONE"));

				list.add(task);
			}
			//con.close();
			return list;
		} catch (Exception e) {
			System.out.println(e);
		}
		return list;
	}

	public static void addNewTask(String task) {

		try {
			Connection con = getConnection();
			PreparedStatement ps = con.prepareStatement("INSERT INTO TO_DO_LIST (LIST, IS_DONE) VALUES (?, ?);");
			ps.setString(1, task);
			ps.setInt(2, 0);
			ps.execute();			
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public static void updateStatusById(int id) {

		try {
			Connection con = getConnection();
			PreparedStatement ps = con.prepareStatement("UPDATE TO_DO_LIST SET is_done = 1 WHERE id = ?;");
			ps.setLong(1, id);
			ps.executeUpdate();

		} catch (Exception e) {
			System.out.println(e);
		}
	}
}
