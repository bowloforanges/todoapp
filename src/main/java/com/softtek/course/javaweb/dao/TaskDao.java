package com.softtek.course.javaweb.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.softtek.course.javaweb.beans.TaskBean;
import com.softtek.course.javaweb.service.ToDoService;

public class TaskDao {

	public TaskDao() {
		super();
		// TODO Auto-generated constructor stub
	}

	public static List<TaskBean> getDoneTasks() {

		List<TaskBean> allTasks = ToDoService.getAllToDo();

		List<TaskBean> doneTasks = allTasks.stream()
			.filter(task -> task.getIs_done().booleanValue())
			.collect(Collectors.toList());

		return doneTasks;
	}

	public static List<TaskBean> getUndoneTasks() {

		List<TaskBean> allTasks = ToDoService.getAllToDo();

		List<TaskBean> undoneTasks = allTasks.stream()
				.filter(task -> task.getIs_done().booleanValue() == false)
				.collect(Collectors.toList());

		return undoneTasks;
	}

	public static List<TaskBean> addTask(String taskstr) {
		// Adds task, then returns undone tasks query
		ToDoService.addNewTask(taskstr);

		List<TaskBean> undoneTasks = getUndoneTasks();
		return undoneTasks;

	}

	public static List<TaskBean> updateStatus(int id) {
		// Executes update, then returns done tasks query
		ToDoService.updateStatusById(id);

		List<TaskBean> doneList = getDoneTasks();
		return doneList;
	}

}
