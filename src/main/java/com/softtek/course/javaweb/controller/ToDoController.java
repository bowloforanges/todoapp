package com.softtek.course.javaweb.controller;

import java.io.IOException;
import com.softtek.course.javaweb.beans.TaskBean;
import com.softtek.course.javaweb.dao.TaskDao;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class toDoController
 */
public class ToDoController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ToDoController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);

		System.out.println("TODOCONTROLLER DEBUGGER");
		
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		List<TaskBean> todolist = TaskDao.getUndoneTasks();
		request.setAttribute("todolist", todolist);
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("views/toDoList.jsp");
		dispatcher.forward(request, response);

	}

}
