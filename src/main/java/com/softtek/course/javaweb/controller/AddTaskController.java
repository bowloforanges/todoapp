package com.softtek.course.javaweb.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.softtek.course.javaweb.beans.TaskBean;
import com.softtek.course.javaweb.dao.TaskDao;

/**
 * Servlet implementation class AddTaskController
 */
public class AddTaskController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddTaskController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);

		String newTask = request.getParameter("taskname");
		TaskDao.addTask(newTask);
		System.out.println(newTask);

		List<TaskBean> todolist = TaskDao.getUndoneTasks();
		request.setAttribute("todolist", todolist);

		RequestDispatcher dispatcher = request.getRequestDispatcher("views/toDoList.jsp");
		dispatcher.forward(request, response);

	}

}
