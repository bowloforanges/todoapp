<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import = "java.util.*" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>To Do:</title>
</head>
<body>

<% 
	//List<TaskBean> todolist = (List<TaskBean>)request.getAttribute("todolist");
%>
	<h2>To Do List</h2><br><hr>

	<table>
		<tr>
			<th>Task ID</th>
			<th>Task</th>
			<th>Manage Status</th>
		</tr>
		<c:forEach items = "${todolist}" var = "u">
			<tr>
			<!-- loop here -->
				<td>${u.getId().toString()}</td>
				<td>${u.getList().toString()}</td>
				<td>
					<form action="./UC" method="GET">
						<button type="submit">Done</button>
						<input type="hidden" name="id" value="${u.getId()}">
					</form>
				</td>
			</tr>
		</c:forEach>
	</table>
	
	<a href="http://localhost:8080/ToDo/">main menu</a>
</body>
</html>